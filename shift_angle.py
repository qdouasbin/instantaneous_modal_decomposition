import numpy as np
from inputParams import *
import treatment as trmt


def get_eqiv_real_proj(ak, phik):
    """
    Computes the projection coefficients corresponding to the real projection (made earlier)
    --> Convinient to compute the shift angle
    ak = complex amp (nb times steps,)
    phik = complex amp (8,)
    """
    print np.shape(ak), ' --> np.shape(ak)'
    print np.shape(phik), ' --> np.shape(phik)'
    alpha, beta = np.real(ak), np.imag(ak)
    gamma, iota = np.mean(np.real(phik)), np.mean(np.imag(phik))

    out = alpha - (beta * iota) / gamma

    return out


def get_angle_shift(a_1T, a_1Tp):
    # Angle using 1T prime
    theta = np.arccos(a_1T / (np.sqrt(np.square(a_1T) + np.square(a_1Tp))))
    theta *= -1
    sintheta = a_1Tp / (np.sqrt(np.square(a_1T) + np.square(a_1Tp)))

    # correcting with initial phi of basis = -2.9 deg
    theta -= 2.9 * np.pi / 180.

    new_theta = []
    for i, s in enumerate(sintheta):
        if s > 0:
            new_theta.append(theta[i])
        else:
            new_theta.append(-theta[i])

    theta = np.array(new_theta)

    phi = []

    for th in theta:
        if th < 0:
            phi.append(np.pi + th)
        else:
            phi.append(th)

    phi = np.array(phi)

    return theta, phi


def get_all_equiv_ak(a1, a2, phi1, phi2):
    eqiv_a1 = get_eqiv_real_proj(a1, phi1)
    eqiv_a2 = get_eqiv_real_proj(a2, phi2)
    return np.array(eqiv_a1), np.array(eqiv_a2)


def clip_low_amp_hilbert_criterion(t, b1, b2):
    """
    t, a1 and a2 are real values. 
    Meaning it should be used  only with equivalent real a_k arrays.
    """
    #       MODIF 1/03/2016
    #       IN order to take also complex signals
    a1 = np.real(b1)
    a2 = np.real(b2)

    # Giving a nice name to the condition
    k = cndtn_percent_peak_value

    # Init output variables
    tout = []
    a1out = []
    a2out = []

    # Getting modulus of H(a), aka the enveloppe
    h_a1 = trmt.getHilbert(a1)
    h_a2 = trmt.getHilbert(a2)

    # epsilon vaut 1 (son maximum) quand les a vaut 0
    eps1 = (h_a1 - np.abs(a1)) / h_a1
    eps2 = (h_a2 - np.abs(a2)) / h_a2

    # Applying criterion at each time
    for i, time in enumerate(t):
        if max(eps1[i], eps2[i]) < k:
            tout.append(time)
            a1out.append(b1[i])
            a2out.append(b2[i])

    # Converting in numpy arrays
    tout = np.array(tout)
    a1out = np.array(a1out)
    a2out = np.array(a2out)

    return tout, a1out, a2out
