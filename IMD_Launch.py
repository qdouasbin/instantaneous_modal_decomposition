"""
i   xnew = np.linspace(t[0], t[-1], len(t))
Main script for running an instantaneous modal decomposition USING COMPLEX NUMBERS !

The first attempt was using real domain projections.

We now use complex decomposition.

The real input data is transformed to complex through Hilbert transform.
The real part of the hilbert transform is the initial real signal.
The modulus of the hilbert transform is the enveloppe.

IMFT
25/01/2016
Quentin Douasbin
quentin.douasbin@imft.fr
"""

import pylab as pl
import scipy.signal as sgnl
import numpy as np
import os as os

import modes as modes
import read_data as r
import treatment as trmt
import write_output as wr
import shift_angle as angle
import cangle as cangle

from shared import *
from inputParams import *

from matplotlib import rcParams
import matplotlib.ticker as tick

rcParams['text.usetex'] = True
rcParams['text.latex.unicode'] = True
params1 = {
    'axes.labelsize': 20,
    #       'text.fontsize': 20,
    'xtick.labelsize': 23,
    'ytick.labelsize': 20,
    #       'legend.pad': 0.1,     # empty space around the legend box
    'legend.fontsize': 23,
    'lines.markersize': 10,
    'font.size': 20,
}
rcParams.update(params1)

rcParams['mathtext.fontset'] = 'stix'
rcParams['font.family'] = 'STIXGeneral'
# pyplot.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')

#########################################
#####       Script starts here       ####
#########################################

if simu:
    data, temporal = r.get_data_simulation(fromLine, upToLine, pathInputSignals)
    data /= 1e5
if xp:
    data, temporal = r.get_data_XP(fromLine, upToLine, pathInputSignals)
if case == 2:
    data, temporal = r.get_data_simulation(fromLine, upToLine, pathInputSignals)

# -----------------------------------------
# Others
# -----------------------------------------
num_probe = 1  # probe in which we plot the signal
fs = (len(temporal) - 1) / (temporal[-1] - temporal[0])  # Sampling frequency
print 'fs = %d' % fs, 'Hz'

### Real data to complex data
# Hilbert transform done on the last dimension --> we need to transpose to treat and transpose back to keep the data structure
cdata = np.transpose(sgnl.hilbert(np.transpose(data)))

# -----------------------------------------
# Data Treatment
# -----------------------------------------

# project the microphones amplitudes into the new base for each temporal times
print
print 'Processing complex signals...'
amplitudes = trmt.treatData(cdata)
print "Signals processed"
print

modesSignal = []

for i in range(3):
    modesSignal.append([])

for temp in amplitudes:
    for i, mode in enumerate(modesSignal):
        mode.append(temp[i])

a1 = npa(modesSignal[0])
a2 = npa(modesSignal[1])
a3 = npa(modesSignal[2])

ModAk = []

for i, a in enumerate(a1):
    p_rms = np.sqrt(0.5 * (np.vdot(a1[i], a1[i]) + np.vdot(a2[i], a2[i])))
    p_env = np.sqrt((np.vdot(a1[i], a1[i]) + np.vdot(a2[i], a2[i])))
    ModAk.append(p_rms)

ampTang = npa(ModAk)
ampTang = ampTang.real

if simu:
    WindowFilterRMS = 701
elif case == 2:
    WindowFilterRMS = 701
elif xp:
    WindowFilterRMS = 1401

orderFilterRMS = 3
filtAmpTang = sgnl.savgol_filter(ampTang, WindowFilterRMS, orderFilterRMS)

modRad = []

for a in a3:
    modRad.append(np.sqrt(0.5 * np.vdot(a, a)))

filtAmpRad = sgnl.savgol_filter(npa(modRad).real, WindowFilterRMS, orderFilterRMS)

count = 0

fig1 = pl.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(1000. * temporal, filtAmpTang, 'k', label=r"$p_{RMS}$", linewidth=2.5)

if not case == 2:
    if simu:
        ax1.set_xlim(0., 7.5)
        ax1.set_ylim(0., 40)
else:
    ax1.set_xlim(0., 40.)

ax1.set_xlabel('Time [ms]')
ax1.set_ylabel('Pressure [Pa]')
# pl.title('$\\varphi = ' + str(angleCase) + '^\circ$')
pl.legend(loc='best')
pl.grid(which='major')
y_fmt = tick.FormatStrFormatter('%0.e')
x_fmt = tick.FormatStrFormatter('%0.f')
ax1.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax1.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
pl.tight_layout(True)

count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

fig2 = pl.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(1000. * temporal, np.real(a1), 'b', label=r"$a_{1T}$", linewidth=1.0)
ax2.plot(1000. * temporal, np.real(a2), 'r', label=r"$a_{1T'}$", linewidth=1.0)
ax2.plot(1000. * temporal, filtAmpTang, 'k', label=r"$p_{RMS}$", linewidth=2.5)
ax2.plot(1000. * temporal, np.sqrt(2) * filtAmpTang, '--k', label=r"$ p_{RMS} \times \sqrt{2}$", linewidth=2.5)
ax2.set_xlim(1000. * np.amin(temporal), 1000. * np.amax(temporal))
pl.grid(True)

if not case == 2:
    if simu:
        ax2.set_xlim(0., 7.5)
        ax2.set_ylim(0., 40)
else:
    ax2.set_xlim(0., 40.)

ax2.set_xlabel('Time [ms]')
ax2.set_ylabel('Pressure [Pa]')
ax2.set_title('Amplitude')
pl.legend(loc='best')
pl.grid(which='major')
y_fmt = tick.FormatStrFormatter('%.e')
x_fmt = tick.FormatStrFormatter('%.f')
ax2.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax2.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
pl.tight_layout(True)

count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

# -----------------------------------------
# Reconstruction of the equivalent signals
# -----------------------------------------

print
print 'Reconstructing the equivalent signal...'

if filt:
    filta1 = trmt.butter_bandpass_filter(np.real(modesSignal[1 - 1]), firstModeFreq - filterWidth / 2.,
                                         maxFiltVal + filterWidth / 2., fs, order=5)
    filta2 = trmt.butter_bandpass_filter(np.real(modesSignal[2 - 1]), firstModeFreq - filterWidth / 2.,
                                         maxFiltVal + filterWidth / 2., fs, order=5)
    filta3 = trmt.butter_bandpass_filter(np.real(modesSignal[3 - 1]), scndModeFreq - filterWidth / 2.,
                                         maxFiltVal + filterWidth / 2., fs, order=5)

    reconstructedFiltered = modes.getReconstructedSignal(filta1, filta2, filta3, temporal)
else:
    reconstructedSignal = modes.getReconstructedSignal(modesSignal[1 - 1], modesSignal[2 - 1], modesSignal[3 - 1],
                                                       temporal)
print 'Signal reconstructed'

print 'Computing 1T angle'

# Carefull : this is for testing rotating basis
if filt:
    tClipC, phiC, phiC2 = cangle.getAngleComplexAmp(temporal, filta1, filta2, filta3)
else:
    tClipC, phiC, phiC2 = cangle.getAngleComplexAmp(temporal, a1, a2, a3)

# nombres de bars sur l'histogramme
factorBar = 1
nb_bars = np.linspace(0, 180, factorBar * 18)
nb_bars_minus = np.linspace(-180, 180, factorBar * 18)

# -------------- TEST COMPLEX -------------- #
pl.figure()
pl.grid(True, which='both')
weightsss = 100. * np.ones_like(phiC2) / len(phiC2)
histo = pl.hist(180. / np.pi * phiC2, nb_bars, normed=None, facecolor='lightgrey', alpha=1,
                rwidth=0.8, zorder=20, weights=weightsss)
# pl.title('Initial Orientation: $\\varphi = ' + str(angleCase) +'^\circ$')
pl.ylabel('Number of Samples [%]')
pl.tight_layout()
pl.xlim(0, 180)
# pl.title('PhiC2')
count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

pl.figure()
pl.grid(True, which='both')
weightsss = 100. * np.ones_like(phiC) / len(phiC)
histo = pl.hist(180. / np.pi * phiC, nb_bars, normed=None, facecolor='lightgrey', alpha=1,
                rwidth=0.8, zorder=20, weights=weightsss)
# pl.title('Initial Orientation: $\\varphi = ' + str(angleCase) +'^\circ$')
pl.ylabel('Number of Samples [%]')
pl.tight_layout()
pl.xlim(0, 180)
# pl.title('PhiC')
count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

if simu:
    WindowFilterAngle = 21
elif case == 2:
    WindowFilterAngle = 21
elif xp:
    WindowFilterAngle = 41
orderFilterAngle = 3
filtAngle = sgnl.savgol_filter(phiC, WindowFilterAngle, orderFilterAngle)

if simu:
    WindowFilterAngle = 21
elif case == 2:
    WindowFilterAngle = 21
elif xp:
    WindowFilterAngle = 41

orderFilterAngle = 3
filtAngle1 = sgnl.savgol_filter(phiC2, WindowFilterAngle, orderFilterAngle)

pl.figure()
pl.plot(1.0e3 * np.array(tClipC), 180. / np.pi * phiC2, 's', color='r', markersize=2)
pl.plot(1.0e3 * np.array(tClipC), 180. / np.pi * filtAngle1, 'k', linewidth=2.5)
pl.tight_layout()
# pl.ylim(-180, 180)

if not case == 2:
    if simu:
        pl.xlim(0., 7.5)
        pl.ylim(0., 40)
else:
    pl.xlim(0., 40.)

pl.ylabel('Orientation $\\varphi$ [$^\circ$]')
pl.xlabel('Time [ms]')
pl.tight_layout()
# pl.title('Initial Orientation: $\\varphi = ' + str(angleCase) +'^\circ$')
# pl.title('PhiC2')
count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

pl.figure()
pl.plot(1.0e3 * np.array(tClipC), 180. / np.pi * phiC, 's', color='r', markersize=2)
pl.plot(1.0e3 * np.array(tClipC), 180. / np.pi * filtAngle, 'k', linewidth=2.5)
# pl.ylim(-180, 180)

if not case == 2:
    if simu:
        pl.xlim(0., 7.5)
        pl.ylim(0., 40)
else:
    pl.xlim(0., 40.)

pl.ylabel('Orientation $\\varphi$ [$^\circ$]')
pl.xlabel('Time [ms]')
pl.tight_layout()
# pl.title('Initial Orientation: $\\varphi = ' + str(angleCase) +'^\circ$')
# pl.title('PhiC')
count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

pl.figure()
pl.plot(1.0e3 * np.array(tClipC), 180. / np.pi * phiC2, 's', color='r', markersize=2)
# pl.ylim(-180, 180)

if not case == 2:
    if simu:
        pl.xlim(0., 7.5)
        pl.ylim(0., 40)
else:
    pl.xlim(0., 40.)

pl.ylabel('Orientation $\\varphi$ [$^\circ$]')
pl.xlabel('Time [ms]')
pl.tight_layout()
# pl.title('Initial Orientation: $\\varphi = ' + str(angleCase) +'^\circ$')
count += 1
if simu:
    pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
elif xp:
    pl.savefig('./FIGURES/XP_' + str(LoadPoint) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)
# -------------- TEST COMPLEX -------------- #


# -----------------------------------------
# Checking % of resolved energy
# -----------------------------------------

if energy:
    print
    print 'Computing energies of the signals'
    print 'RMS(XP) :', trmt.rms(data)
    if filt:
        print 'RMS(reconstructed)', trmt.rms(reconstructedFiltered)
        print '% of resolved energy : ', trmt.rms(reconstructedFiltered) / trmt.rms(data)
        print '% of resolved energy unfiltered: ', trmt.rms(reconstructedSignal) / trmt.rms(data)

# -----------------------------------------
# Plotting outputs
# -----------------------------------------

if plots:
    # plotting the results
    fig, axarr = pl.subplots(2, 2)

    if raw:
        axarr[0, 0].plot(1000. * temporal, np.real(modesSignal[1 - 1]), 'b', label='Raw $1T$', linewidth=1.5)
        # axarr[0, 0].plot(1000. * temporal, np.imag(modesSignal[1 - 1]), 'b+', label='imag Raw $1T$', linewidth=1.5)
        axarr[0, 0].plot(1000. * temporal, np.real(modesSignal[2 - 1]), 'r', label='Raw $1T\'$', linewidth=1.5)
        # axarr[0, 0].plot(1000. * temporal, np.imag(modesSignal[2 - 1]), 'r+', label='imagRaw $1T\'$', linewidth=1.5)
        axarr[0, 0].plot(1000. * temporal, np.real(modesSignal[3 - 1]), 'g', label='Raw $1R$', linewidth=1.5)
        # axarr[0, 0].plot(temporal, 1e-5 * (np.array(modesSignal[1 - 1]) + np.array(modesSignal[2 - 1]) + np.array(modesSignal[3 - 1])), 'k', label='sum', linewidth=1.5)

    if filt:
        axarr[0, 0].plot(temporal, 1e-5 * filta1, 'b--', linewidth=1., label='Filt. $1T$')
        axarr[0, 0].plot(temporal, 1e-5 * filta2, 'r--', linewidth=1., label='Filt. $1T\'$')
        axarr[0, 0].plot(1000. * temporal, filta3, 'g--', linewidth=1., label='Filt. $1R$')

    axarr[0, 0].set_xlim(1000. * np.amin(temporal), 1000. * np.amax(temporal))
    axarr[0, 0].grid(True)
    axarr[0, 0].set_xlabel('Time [ms]')
    axarr[0, 0].set_ylabel('Pressure [Pa]')
    axarr[0, 0].set_title('Instantaneous amplitudes of $1T$, $1T\'$ and $1R$')
    axarr[0, 0].legend(loc='best')

    if fft:
        print 'Performing FFTs'
        padfactor = 1
        nfft = padfactor * len(modesSignal[1 - 1])

        if raw:
            ffta1 = sgnl.welch(np.real(modesSignal[1 - 1]), fs, nfft=nfft)
            ffta2 = sgnl.welch(np.real(modesSignal[2 - 1]), fs, nfft=nfft)
            ffta3 = sgnl.welch(np.real(modesSignal[3 - 1]), fs, nfft=nfft)
            fftRecSign = sgnl.welch(np.real(reconstructedSignal[:, num_probe]), fs, nfft=nfft)
            axarr[1, 1].semilogy(ffta1[0], ffta1[1], 'b', label='$a_1$', linewidth=1.5)
            axarr[1, 1].semilogy(ffta2[0], ffta2[1], 'r', label='$a_2$', linewidth=1.5)
            axarr[1, 1].semilogy(ffta3[0], ffta3[1], 'g', label='$a_3$', linewidth=1.5)
            axarr[1, 1].semilogy(fftRecSign[0], fftRecSign[1], 'k', label='Rec', linewidth=1.5)

            # if filt:
            # fftFilt1 = sgnl.welch(filta1, fs, nfft=nfft)
            # fftFilt2 = sgnl.welch(filta2, fs, nfft=nfft)
            # fftFilt3 = sgnl.welch(filta3, fs, nfft=nfft)
            # axarr[1, 1].semilogy(fftFilt1[0], fftFilt1[1], 'b--', label='Filt. $a_1$', linewidth=1.5)
            # axarr[1, 1].semilogy(fftFilt2[0], fftFilt2[1], 'r--', label='Filt. $a_2$', linewidth=1.5)
            # axarr[1, 1].semilogy(fftFilt3[0], fftFilt3[1], 'g--', label='Filt. $a_3$', linewidth=1.5)

        axarr[1, 1].grid(True)
        axarr[1, 1].set_xlabel('Frequency [Hz]')
        axarr[1, 1].set_ylabel('[Pa/Hz]')
        axarr[1, 1].set_title('PSD of the amplitudes')
        axarr[1, 1].legend(loc='best')

    axarr[0, 1].plot(1000. * temporal, data[:, num_probe], color='#3465a4', label='Signal', linewidth=2.)
    axarr[0, 1].plot(1000. * temporal, np.real(cdata[:, num_probe]), 'k+', label='real CSignal', linewidth=2.)
    # axarr[0, 1].plot(1000. * temporal, np.imag(cdata[:, num_probe]), 'k--', label='imag CSignal', linewidth=2.)

    if raw:
        axarr[0, 1].plot(1000. * temporal, np.real(reconstructedSignal[:, num_probe]), 'r+', linewidth=1,
                         label='Abs Reconstructed')
        axarr[0, 1].plot(1000. * temporal, np.abs(reconstructedSignal[:, num_probe]), 'r', linewidth=3,
                         label='Abs Reconstructed')
        # axarr[0, 1].plot(1000. * temporal, np.imag(reconstructedSignal[:, num_probe]), 'r--', linewidth=1,
        #                  label=' Imag Reconstructed')
        # if filt:
        # axarr[0, 1].plot(temporal, reconstructedFiltered[:, num_probe], 'k--', linewidth=0.7, label='Rct. from filt.')

    axarr[0, 1].set_xlim(1000. * np.amin(temporal), 1000. * np.amax(temporal))
    axarr[0, 1].grid(True)
    axarr[0, 1].set_xlabel('Time [ms]')
    axarr[0, 1].set_ylabel('Pressure [Pa]')
    axarr[0, 1].set_title('Pressure signals')
    axarr[0, 1].legend(loc='best')


    def env(x):
        out = np.sqrt(np.square(np.real(x)) + np.square(np.imag(x)))
        return out


    if raw:
        axarr[1, 0].plot(1000. * temporal, 0.5 * np.abs(modesSignal[1 - 1]), 'b--', linewidth=0.7, label='$A_{1T}$')
        axarr[1, 0].plot(1000. * temporal, 0.5 * np.abs(modesSignal[2 - 1]), 'k+--', linewidth=0.7,
                         label="""$A_{1T'}$""")
        axarr[1, 0].plot(1000. * temporal, 0.5 * np.abs(modesSignal[3 - 1]), 'g--', linewidth=0.7, label='$A_{1R}$')
        axarr[1, 0].plot(1000. * temporal, np.abs(reconstructedSignal[:, num_probe]), 'r+', linewidth=0.7,
                         label="""Rec""")
    if filt:
        # axarr[1, 0].plot(1000.*temporal, 1e-5 *trmt.getHilbert(filta1), 'b--', linewidth=1.5, label='Filt $A_{1T}$')
        # axarr[1, 0].plot(1000.*temporal, 1e-5 *trmt.getHilbert(filta2), 'r--', linewidth=1.5, label="""Filt $A_{1T'}$""")
        axarr[1, 0].plot(1000. * temporal, np.abs(filta3), 'g', linewidth=1.5, label='Filt $A_{1R}$')

    axarr[1, 0].set_xlim(1000. * np.amin(temporal), 1000. * np.amax(temporal))
    axarr[1, 0].grid(True)
    axarr[1, 0].set_xlabel('Time [ms]')
    axarr[1, 0].set_ylabel('Pressure [Pa]')
    axarr[1, 0].set_title('Envelopes')
    axarr[1, 0].legend(loc='best')

pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) + '.png', transparent=True, dpi=400)

# -----------------------------------------
# Exporting the output data
# -----------------------------------------


if data_export:
    print
    print 'Export of data...'
    # Exporting Modulus of Hilbert
    if False:
        wr.export(nameOutput + '_Enveloppe', './OUTPUT_IMD/', temporal,
                  (np.abs(np.array(modesSignal[1 - 1])) ** 2 +
                   np.abs(np.array(modesSignal[2 - 1])) ** 2) ** 0.5,
                  np.array(modesSignal[1]) / np.array(modesSignal[1]),
                  np.array(modesSignal[1]) / np.array(modesSignal[1]))
        pl.figure(12354)
        pl.plot(temporal,
                (np.abs(np.array(modesSignal[1 - 1])) ** 2 + np.abs(np.array(modesSignal[2 - 1])) ** 2) ** 0.5,
                label='Norm of a1 + a2')
        pl.plot(temporal, np.abs(np.array(modesSignal[1 - 1]) +
                                 np.array(modesSignal[2 - 1])), label='Norm (a1 + a2)')
        #        pl.plot(temporal, (np.abs(filta1)**2 + np.abs(filta2)**2)**0.5,
        #               label='filt Norm of a1 + a2')
        pl.legend()

    print 'Export Done'
    print
    print 'Plotting...'

if case == 2:
    wr.export('ComplexAmplitudes' + '_TestCase_' + str(angleCase),
              './OUTPUT_IMD/ComplexAmplitude' + '_TestCase_' + str(angleCase), temporal, a1, a2, a3)
    wr.export4plot('Amplitude', './OUTPUT_IMD/Amplitude_TestCase_' + str(angleCase), temporal, filtAmpTang,
                   'Time [s], Amplitude of 1T [Pa]')
    wr.export4plot('UnfittedAngle1', './OUTPUT_IMD/Angle_TestCase_' + str(angleCase), tClipC, (180. / np.pi) * phiC,
                   'Time [s], Angle of nodal line [deg]')
    wr.export4plot('UnfittedAngle2', './OUTPUT_IMD/Angle_TestCase_' + str(angleCase) + '_2', tClipC,
                   (180. / np.pi) * phiC2, 'Time [s], Angle of nodal line [deg]')
    wr.export4plot('FitAngle1', './OUTPUT_IMD/Angle_TestCase_' + str(angleCase), tClipC, (180. / np.pi) * filtAngle,
                   'Time [s], Angle of nodal line [deg]')
    wr.export4plot('FitAngle2', './OUTPUT_IMD/Angle_TestCase_' + str(angleCase) + '_2', tClipC,
                   (180. / np.pi) * filtAngle1, 'Time [s], Angle of nodal line [deg]')
if simu:
    wr.export('ComplexAmplitudes' + '_PhiLES_' + str(angleCase),
              './OUTPUT_IMD/ComplexAmplitude' + '_PhiLES_' + str(angleCase), temporal, a1, a2, a3)
    wr.export4plot('Amplitude', './OUTPUT_IMD/Amplitude_PhiLES_' + str(angleCase), temporal, filtAmpTang,
                   'Time [s], Amplitude of 1T [Pa]')
    wr.export4plot('UnfittedAngle1', './OUTPUT_IMD/Angle_PhiLES_' + str(angleCase), tClipC, (180. / np.pi) * phiC,
                   'Time [s], Angle of nodal line [deg]')
    wr.export4plot('UnfittedAngle2', './OUTPUT_IMD/Angle_PhiLES_' + str(angleCase) + '_2', tClipC,
                   (180. / np.pi) * phiC2, 'Time [s], Angle of nodal line [deg]')
    wr.export4plot('FitAngle1', './OUTPUT_IMD/Angle_PhiLES_' + str(angleCase), tClipC, (180. / np.pi) * filtAngle,
                   'Time [s], Angle of nodal line [deg]')
    wr.export4plot('FitAngle2', './OUTPUT_IMD/Angle_PhiLES_' + str(angleCase) + '_2', tClipC,
                   (180. / np.pi) * filtAngle1, 'Time [s], Angle of nodal line [deg]')
if xp:
    if preProcXPData:
        namexp = '_XP_preprocessed_' + LoadPoint
        wr.export('ComplexAmplitudes' + namexp, './OUTPUT_IMD/ComplexAmplitude', temporal, a1, a2, a3)
        wr.export4plot('Amplitude', './OUTPUT_IMD/Amplitude' + namexp, temporal, filtAmpTang,
                       'Time [s], Amplitude of 1T [Pa]')
        wr.export4plot('UnfittedAngle1', './OUTPUT_IMD/Angle' + namexp, tClipC, (180. / np.pi) * phiC,
                       'Time [s], Angle of nodal line [deg]')
        wr.export4plot('UnfittedAngle2', './OUTPUT_IMD/Angle' + namexp + '_2', tClipC, (180. / np.pi) * phiC2,
                       'Time [s], Angle of nodal line [deg]')

        wr.export4plot('FitAngle1', './OUTPUT_IMD/Angle' + namexp, tClipC, (180. / np.pi) * filtAngle,
                       'Time [s], Angle of nodal line [deg]')
        wr.export4plot('FitAngle2', './OUTPUT_IMD/Angle' + namexp + '_2', tClipC, (180. / np.pi) * filtAngle1,
                       'Time [s], Angle of nodal line [deg]')


    else:
        namexp = '_XP_' + LoadPoint
        wr.export('ComplexAmplitudes' + namexp, './OUTPUT_IMD/ComplexAmplitude', temporal, a1, a2, a3)
        wr.export4plot('Amplitude', './OUTPUT_IMD/Amplitude' + namexp, temporal, filtAmpTang,
                       'Time [s], Amplitude of 1T [Pa]')
        wr.export4plot('UnfittedAngle1', './OUTPUT_IMD/Angle' + namexp, tClipC, (180. / np.pi) * phiC,
                       'Time [s], Angle of nodal line [deg]')
        wr.export4plot('UnfittedAngle2', './OUTPUT_IMD/Angle' + namexp + '_2', tClipC, (180. / np.pi) * phiC2,
                       'Time [s], Angle of nodal line [deg]')

        wr.export4plot('FitAngle1', './OUTPUT_IMD/Angle' + namexp, tClipC, (180. / np.pi) * filtAngle,
                       'Time [s], Angle of nodal line [deg]')
        wr.export4plot('FitAngle2', './OUTPUT_IMD/Angle' + namexp + '_2', tClipC, (180. / np.pi) * filtAngle1,
                       'Time [s], Angle of nodal line [deg]')

print 'End of the program'
