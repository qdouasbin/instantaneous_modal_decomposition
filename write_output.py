"""
IMFT
09/11/2015
Quentin Douasbin
quentin.douasbin@imft.fr
"""

import os as os

import numpy as np


def export(flnm, dirnm_out, t, a1, a2, a3):
    # creates directory if not already existing
    if not os.path.exists(dirnm_out):
        os.makedirs(dirnm_out)
    t = np.transpose(t)
    filename = './' + dirnm_out + '/' + flnm + '.ascii'
    np.savetxt(filename,
               np.column_stack((t, np.real(a1), np.imag(a1), np.real(a2), np.imag(a2), np.real(a3), np.imag(a3))),
               header='t, real(a1), imag(a1), real(a2), imag(a2), real(a3), imag(a3)', )


def export_1T_angle(flnm, dirnm_out, t, angle):
    # creates directory if not already existing
    if not os.path.exists(dirnm_out):
        os.makedirs(dirnm_out)
    t = np.transpose(t)
    flnm = '1T_angle_' + flnm
    filename = './' + dirnm_out + '/' + flnm + '.ascii'
    np.savetxt(filename, np.column_stack((t, np.round(angle, 3))),
               header='t, angle (deg) -------> !!!! Attention definition de l''angle pas verifiee', )


def export4plot(flnm, dirnm_out, t, var, hdr):
    # creates directory if not already existing
    if not os.path.exists(dirnm_out):
        os.makedirs(dirnm_out)
    t = np.transpose(t)
    filename = './' + dirnm_out + '/' + flnm + '.ascii'
    np.savetxt(filename, np.column_stack((t, var)), header=hdr)
