"""
IMFT
09/11/2015
Quentin Douasbin
quentin.douasbin@imft.fr
"""

import numpy as np
from numpy import linalg as la
from inputParams import *
import cmath as c


def getReconstructedSignal(a1, a2, a3, temporal):
    """

    :param a1: Complex amplitude of mode 1T
    :param a2: Complex amplitude of mode 1T'
    :param a3: Complex amplitude of mode 1R
    :param temporal: 1D array corresponding to time
    :return: recontructed array
    """

    out = []

    for i, t in enumerate(temporal):
        sig = a1[i] * vec_phi_l_from_file(1) + a2[i] * vec_phi_l_from_file(2) + a3[i] * vec_phi_l_from_file(3)
        out.append(sig)

    return np.array(out)


def vec_phi_l_from_file(l):
    """
    Gets complex modes
    :param l:
    :return:
    """

    pathModes = [pathMode1, pathMode2, pathMode3]

    p = np.loadtxt(pathModes[l - 1], skiprows=1)
    p = np.transpose(p)
    pReal = p[0]
    pImag = p[1]

    p = pReal + pImag * 1j
    #   print 'Norm of', [l-1], '\t-->\t', np.sqrt(np.vdot(p, p))


    ## !! COMPLEX NORM !! ## --> Int( (conj(a)) * (b) * dV)
    pNormed = p / np.sqrt(np.vdot(p, p))
    pNormed = normVec(p)

    #   print 'Norm of pNormed -->', np.vdot(pNormed, pNormed)

    return pNormed


def normVec(vec):
    vecNormed = vec / np.sqrt(np.vdot(vec, vec))
    return vecNormed


def getNorm(vec):
    Norm = np.sqrt(np.vdot(vec, vec))
    return Norm
