# Which kind of input ? XP or SIMULATION ?
# 0 --> XP, 1 --> SIMULATION, 2 --> TEST-CASE
case = 1

# Choosing angle case --> phidlr
angleCase = 135

# Choosing angle case --> phidlr
LoadPoint = 'LP4'
LoadPoint = 'LP1'

# ---------------------------------
# !! PreProcessing DLR --> filter !!
preProcXPData = True
preProcXPData = False
# ---------------------------------
if case == 1:
    xp, simu = False, True
elif case == 0:
    simu, xp = False, True
elif case == 2:
    simu, xp = True, False

# -----------------------------------------
# Selective entry for the program
# -----------------------------------------
# True if you're willing to check the resolved energy, False otherwise
energy = False

# True if you're willing to plot the results, False otherwise
plots = True

# True if you're willing to export the output, False otherwise
data_export = True

# True if you're willing to check frequency corresponding to the input modes, False otherwise
fft = False

# True if you're willing to visualize the raw data (= unfiltered), False otherwise
raw = True

# True if you're willing to visualize the filtered data (around corresponding peaks), False otherwise
filt = False

# -----------------------------------------
# Defining selective data reading case
# -----------------------------------------
# # Read data in file from line ... up to line ...
if simu:
    fromLine = 30
    upToLine = -1
elif xp:
    if LoadPoint == 'LP1':
        fromLine = 0
        upToLine = -1
    if LoadPoint == 'LP4':
        fromLine = 600000
        upToLine = 700001
        if preProcXPData:
            fromLine = 0
            upToLine = -1

# -----------------------------------------
# Defining IO Paths
# -----------------------------------------
if simu:

    if angleCase == 0:
        cndtn_percent_peak_value = 0.0001
        pathInputSignals = './INPUT_IMD/IM10_PHI0_PROBES/p_'
        nameOutput = 'Inst_Amp_IM10_PhiLES0'

    elif angleCase == 45:
        pathInputSignals = './INPUT_IMD/IM10_PHI45_PROBES/p_'
        nameOutput = 'Inst_Amp_IM10_PhiLES45'
        cndtn_percent_peak_value = 0.0005

    elif angleCase == 90:
        pathInputSignals = './INPUT_IMD/IM10_PHI90_PROBES/p_'
        nameOutput = 'Inst_Amp_IM10_PhiLES90'
        cndtn_percent_peak_value = 0.0005

    elif angleCase == 135:
        pathInputSignals = './INPUT_IMD/IM10_PHI135_PROBES/p_'
        nameOutput = 'Inst_Amp_IM10_PhiLES135'
        cndtn_percent_peak_value = 0.0005

elif xp:
    angleCase = 'XP'
    pathInputSignals = './INPUT_IMD/XP_DATA/'

    # Defining output and data selection params
    if LoadPoint == 'LP1':
        if preProcXPData:
            cndtn_percent_peak_value = 1e-3
            nameOutput = 'Inst_Amp_XP_LP1_Filtered'
        else:
            cndtn_percent_peak_value = 1e-3
            nameOutput = 'Inst_Amp_XP_LP1_Unfiltered'
    elif LoadPoint == 'LP4':
        cndtn_percent_peak_value = 1e-3
        if preProcXPData:
            nameOutput = 'Inst_Amp_XP_LP4_Filtered'
        else:
            pathInputSignals = './INPUT_IMD/XP_DATA/'
            nameOutput = 'Inst_Amp_XP_LP4_Unfiltered'

# Input basis
pathMode1 = './INPUT_IMD/MODES/complex_phi1.ascii'
pathMode2 = './INPUT_IMD/MODES/complex_phi2.ascii'
pathMode3 = './INPUT_IMD/MODES/complex_phi3.ascii'

if case == 2:
    fromLine = 0
    upToLine = -1
    cndtn_percent_peak_value = 1.e-5
    pathInputSignals = './INPUT_IMD/AMT_1T_0_1_Pa_DAMPED_LONG/'
    nameOutput = 'TEST_CASE_AMT_0_1_Pa_DAMPED_LONG'
    angleCase = '_TestCase'
