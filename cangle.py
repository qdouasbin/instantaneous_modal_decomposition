# PYTHON LIBS
import numpy as np
import treatment as trmt
import matplotlib.pyplot as pl

# HOMEMADE MODULES
import modes as m
import shift_angle as angle
import treatment as trmt
from shared import *

# INPUT FILE
from inputParams import *

def getAngleComplexAmp(t, a1, a2, a3):
    """
    t, a1 and a2 are complex values. 
    """
    #a1, a2, dummy = amps
    a1 = npa(a1)
    a2 = npa(a2)
    a3 = npa(a3)

    mode1 = 0.0 + 1j
    mode2 = 1.0 + 0j

    nodalLineRec = (np.real(a1) * mode1 + np.real(a2)*mode2)

    tClipped, nodalLineRecClipped, dummy = angle.clip_low_amp_hilbert_criterion(t, nodalLineRec, nodalLineRec)

    nodalLineRecClipped = npa(nodalLineRecClipped)

    print 'shape nodalLineRec', np.shape(nodalLineRec)

    ArgDiff = - np.angle( nodalLineRecClipped/np.real(nodalLineRecClipped) / 1j)

    print 'shape ArgDiff', np.shape(ArgDiff)

#   pl.figure()
#   pl.plot(t, nodalLineRec, label='Rec')
#   pl.plot(tClipped, nodalLineRecClipped, 's', markersize=5, label='Clipped data')
#   #pl.plot(tClipped, np.abs(np.abs(a1) + np.abs(a2)), 's', markersize=4, label='Amp(1T)')
#   pl.legend(loc='best')
#   ##pl.show()

    phi = []

    for th in ArgDiff :
        tt = abs(th)
        if th > np.pi/2:
            phi.append(tt - np.pi)
        elif th < -np.pi/2:
            phi.append(np.pi - tt)
        else :
            phi.append(th)

    phi = npa(phi)


#   pl.plot(180. * ArgDiff / np.pi)
#   pl.plot(180. * phi / np.pi)
#   #pl.show()

    return tClipped, ArgDiff, phi


#   def clipLowAmpHilbertCriteriaRotating(t, b1, b2, c1, c2):
#       """
#       t, a1 and a2 are real values. 
#       Meaning it should be used  only with equivalent real a_k arrays.
#       """
#   #       MODIF 1/03/2016
#   #       IN order to take also complex signals
#       a1 = np.real(b1)
#       a2 = np.real(b2)


#       # Giving a nice name to the condition
#       k = cndtn_percent_peak_value

#       # Init output variables
#       tout = []
#       a1out = []
#       a2out = []
#       c1out = []
#       c2out = []

#       # Getting modulus of H(a), aka the enveloppe
#       h_a1 =  trmt.getHilbert(a1)
#       h_a2 =  trmt.getHilbert(a2)

#       # epsilon vaut 1 (son maximum) quand les a vaut 0
#       eps1 = (h_a1 - np.abs(a1)) / h_a1
#       eps2 = (h_a2 - np.abs(a2)) / h_a2

#       # Applying criterion at each time
#       for i, time in enumerate(t):
#           if max(eps1[i], eps2[i]) < k:
#               tout.append(time)
#               a1out.append(b1[i])
#               a2out.append(b2[i])
#               c1out.append(c1[i])
#               c2out.append(c2[i])

#       # Converting in numpy arrays
#       tout = np.array(tout)
#       a1out = np.array(a1out)
#       a2out = np.array(a2out)

#       return tout, a1out, a2out, c1out, c2out


#   def gpeakAmplitudeetSignalForClipping(t, a1, a2, a3):
#       """
#       t, a1 and a2 are complex values. 
#       """
#       #a1, a2, dummy = amps
#       a1 = npa(a1)
#       a2 = npa(a2)
#       a3 = npa(a3)


#       mode1 = 0.0 + 1j
#       mode2 = 1.0 + 0j

#       nodalLineRec = (np.real(a1) * mode1 + np.real(a2)*mode2)

#   # ------------ Test --------------------#
#       nodalLineRec = (np.real(a1) * mode1 / 1.92645 + np.real(a2) * mode2 / 1.722343)
#       nodalLineRec = (np.real(a2) * mode2 / 1.92645 + np.real(a1) * mode1 / 1.722343)
#   # ------------ Test --------------------#

#       tClipped, nodalLineRecClipped, dummy = angle.clip_low_amp_hilbert_criterion(t, nodalLineRec, nodalLineRec)

#       nodalLineRecClipped = npa(nodalLineRecClipped)

#   #   print 'nodalLineRecClipped', nodalLineRecClipped
#       print 'shape nodalLineRec', np.shape(nodalLineRec)

#       ArgDiff = -np.angle( nodalLineRecClipped/np.real(nodalLineRecClipped) / 1j)
#   #   ArgDiff -= ArgDiff[0]
#       #ArgDiff -= np.abs(angleCase) * np.pi / 180.
#   #   ArgDiff = np.angle( nodalLineRecClipped/np.real(nodalLineRecClipped) / mode1)


#       phi = []

#       print 'shape ArgDiff', np.shape(ArgDiff)

#       pl.figure()
#       pl.plot(t, nodalLineRec, label='Rec')
#       pl.plot(tClipped, nodalLineRecClipped, 's', markersize=5, label='Clipped data')
#       #pl.plot(tClipped, np.abs(np.abs(a1) + np.abs(a2)), 's', markersize=4, label='Amp(1T)')
#       pl.legend(loc='best')
#       ##pl.show()

#       for th in ArgDiff:
#           if th < 0:
#               phi.append(np.pi + th)
#           else:
#               phi.append(th)

#       phi = npa(phi)


#       return tClipped, ArgDiff, phi

#   def ClippingRoutine(t, sig, array):
#       """
#       t, a1 and a2 are real values. 
#       Meaning it should be used  only with equivalent real a_k arrays.
#       """
#   #       MODIF 1/03/2016
#   #       IN order to take also complex signals
#       a1 = np.real(sig)
#       

#       # Giving a nice name to the condition
#       k = 0.1*cndtn_percent_peak_value

#       # Init output variables
#       tout = []
#       a1out = []
#       a2out = []

#       # Getting modulus of H(a), aka the enveloppe
#       h_a1 =  trmt.getHilbert(a1)

#       # epsilon vaut 1 (son maximum) quand les a vaut 0
#       eps1 = (h_a1 - np.abs(a1)) / h_a1

#       # Applying criterion at each time
#       for i, time in enumerate(t):
#           if eps1[i] < k:
#               tout.append(time)
#               a1out.append(sig[i])
#               a2out.append(array[i])

#       # Converting in numpy arrays
#       tout = np.array(tout)
#       a1out = np.array(a1out)
#       a2out = np.array(a2out)

#       return tout, a1out, a2out


#   def getAngleComplexAmpRotatingBasis(t, a1, a2, a3):
#       """
#       t, a1 and a2 are complex values. 
#       """
#       a1 = npa(a1)
#       a2 = npa(a2)
#       a3 = npa(a3)
#       
#       b1Hat = 0.5 * (a1 - 1j * a2)
#       b2Hat = 0.5 * (a1 + 1j * a2)
#       
#       b1 = np.abs(b1Hat)
#       b2 = np.abs(b2Hat)

#       beta1 = np.angle(b1Hat)
#       beta2 = np.angle(b2Hat)

#       gamma = 0.5*(beta1 + beta2)
#           
#       pl.plot(gamma)
#       pl.show()

#       c = np.cos(gamma)
#       s = np.sin(gamma)

#       A = c * (b1 + b2)
#       B = s * (b2 - b1)

#       mode1 = 0.0 + 1j
#       mode2 = 1.0 + 0j

#       print 'A = ', np.mean(A) 
#       print 'B = ', np.mean(B) 

#       pReal = A + B
#       pRealMax = np.sqrt(np.square(A) + np.square(B))
#       pReal = pRealMax

#       #tClippedPReal, pRealClipped, dummy = angle.clip_low_amp_hilbert_criterion(t, pReal, pReal)
#       tClippedPReal, pRealClipped, dummy = ClippingRoutine(t, pReal, pReal)
#       tClipped, dummy, AClipped = ClippingRoutine(t, pReal, A)
#       tClipped, dummy, BClipped = ClippingRoutine(t, pReal, B)
#       tClipped, dummy, beta1Clipped = ClippingRoutine(t, pReal, beta1)
#       tClipped, dummy, beta2Clipped = ClippingRoutine(t, pReal, beta2)
#      # tClipped, AClipped, BClipped = angle.clip_low_amp_hilbert_criterion(t, A, B)

#       pl.figure()
#       pl.plot(t, pReal, label='pReal')
#       pl.plot(t, pRealMax, label='pRealMax')
#       pl.title('Rotating Basis Max Press')
#       count = 1589
#       pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) +'.png', transparent=True, dpi=200)
#       #pl.show()
#       pl.figure()
#       pl.plot(tClippedPReal, pRealClipped, 's', markersize=2, label='pRealClipped')
#       pl.title('Clipped pReal')
#       count = 1689
#       pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) +'.png', transparent=True, dpi=200)
#       #pl.show()
#       pl.figure()
#       pl.plot(tClipped, AClipped, 's', markersize=2, label='pRealClipped')
#       pl.plot(tClipped, BClipped, 's', markersize=2, label='pRealClipped')
#       pl.title('Clipped pReal')
#       count = 1689
#       pl.savefig('./FIGURES/PhiLES_' + str(angleCase) + 'figure_' + str(count) +'.png', transparent=True, dpi=200)
#       #pl.show()

#   #   ArgDiff = np.pi - np.arctan(A / B)  + 0.5 * (beta1 - beta2)
#   #   ArgDiff = np.exp(1j*np.arctan(-A / B)) * np.exp(1j * (beta1 - beta2))
#       ArgDiffClipped = np.angle(np.exp(1j*np.arctan(-AClipped / BClipped)) * np.exp(1j * (beta1Clipped - beta2Clipped)))
#       #ArgDiff = np.pi - np.arctan(AClipped / BClipped) + np.pi/2.

#       #tClippedArg, dummy, ArgDiffClipped = ClippingRoutine(t, pRealMax, ArgDiff)
#       phi = []

#       for th in ArgDiffClipped:
#           if th > 0:
#               if th > np.pi/2:
#                   phi.append(np.pi - th)
#               else :
#                   phi.append(th)
#           elif th < 0:
#               if th < -np.pi/2:
#                   phi.append(np.pi + th)
#               else :
#                   phi.append(th)
#           else:
#               phi.append(th)

#       phi = npa(phi)

#       pSh(phi, 'phi')
#       pSh(ArgDiffClipped, 'ArgDiffClipped')
#       pSh(tClipped, 'tClipped')

#       pl.figure()
#       pl.plot(1000. * tClipped, 180. / 3.14 * phi)
#       pl.plot(1000. * tClipped, 180. / 3.14 * ArgDiffClipped)
#       pl.plot(1000. * tClipped, 180. / 3.14 * np.abs(ArgDiffClipped))
#       pl.title('Angles')
#       count = 5589
#       pl.show()
#       #pl.show()

#       return tClipped, ArgDiffClipped, phi


