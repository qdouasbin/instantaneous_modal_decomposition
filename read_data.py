__author__ = 'qdouasbi'

"""
IMFT
09/11/2015
Quentin Douasbin
quentin.douasbin@imft.fr
"""
import numpy as np
import treatment as trmt
import matplotlib.pyplot as pl

from inputParams import *
from shared import *


def read_data(name, path, begin, end, skp):
    out = []
    print
    for filename in name:
        print 'Loading', filename, 'from line', begin, 'to line', end
        data = np.loadtxt(path + filename, skiprows=skp)
        p_t_pr = data[begin:end, 1]  # p(t) at probe
        out.append(p_t_pr)

    print

    temporal = data[begin:end, 0]  # time vector  # Assuming it is the same everywhere

    out = np.array(out) - np.mean(out)

    pSh(temporal, 'temporal')
    pSh(out, 'out')
    pl.show()

    return np.transpose(out), temporal


def filtDataXP(data):
    fs = 100000.
    #   pl.plot(data)
    print 'Filtering input data'
    out = trmt.butter_bandpass_filter(data, lowCut, highCut, fs, order=5)
    pl.plot(out)
    return out


def get_data_XP(begin, end, path):
    """
    Function to load the XP data
    :return: 2D Array of pressure*probes , 1D array of time
    """

    if preProcXPData:
        if LoadPoint == 'LP1':
            # #### ATTENTION !! REPARTITION NON TRIVIALE --> cf 1T Mode Orientation Groning ####
            name = ['HF-7-LP1-pdyn7-pp.ascii', 'HF-7-LP1-pdyn8-pp.ascii', 'HF-7-LP1-pdyn1-pp.ascii',
                    'HF-7-LP1-pdyn2-pp.ascii',
                    'HF-7-LP1-pdyn3-pp.ascii', 'HF-7-LP1-pdyn4-pp.ascii', 'HF-7-LP1-pdyn5-pp.ascii',
                    'HF-7-LP1-pdyn6-pp.ascii']

        elif LoadPoint == 'LP4':
            # #### ATTENTION !! REPARTITION NON TRIVIALE --> cf 1T Mode Orientation Groning ####
            name = ['HF-7-LP4-pdyn7-pp.ascii', 'HF-7-LP4-pdyn8-pp.ascii', 'HF-7-LP4-pdyn1-pp.ascii',
                    'HF-7-LP4-pdyn2-pp.ascii',
                    'HF-7-LP4-pdyn3-pp.ascii', 'HF-7-LP4-pdyn4-pp.ascii', 'HF-7-LP4-pdyn5-pp.ascii',
                    'HF-7-LP4-pdyn6-pp.ascii']
    else:
        if LoadPoint == 'LP1':
            # #### ATTENTION !! REPARTITION NON TRIVIALE --> cf 1T Mode Orientation Groning ####
            name = ['HF-7-LP1-pdyn7.ascii', 'HF-7-LP1-pdyn8.ascii', 'HF-7-LP1-pdyn1.ascii', 'HF-7-LP1-pdyn2.ascii',
                    'HF-7-LP1-pdyn3.ascii', 'HF-7-LP1-pdyn4.ascii', 'HF-7-LP1-pdyn5.ascii', 'HF-7-LP1-pdyn6.ascii']

        elif LoadPoint == 'LP4':
            # #### ATTENTION !! REPARTITION NON TRIVIALE --> cf 1T Mode Orientation Groning ####
            name = ['HF-7-LP4-pdyn7.ascii', 'HF-7-LP4-pdyn8.ascii', 'HF-7-LP4-pdyn1.ascii', 'HF-7-LP4-pdyn2.ascii',
                    'HF-7-LP4-pdyn3.ascii', 'HF-7-LP4-pdyn4.ascii', 'HF-7-LP4-pdyn5.ascii', 'HF-7-LP4-pdyn6.ascii']

    return read_data(name, path, begin, end, 0)


def get_data_simulation(begin, end, path):
    """
    Function to load the simulation data
    :return: 2D Array of pressure*probes , 1D array of time
    """
    probes_names = []

    for name in ['C1.dat', 'C2.dat', 'C3.dat', 'C4.dat', 'C5.dat', 'C6.dat', 'C7.dat', 'C8.dat']:
        probes_names.append(name)

    print "\nPath of the input data: %s\nName of the input probes:%s" % (path, probes_names)

    return read_data(probes_names, path, begin, end, 1)
