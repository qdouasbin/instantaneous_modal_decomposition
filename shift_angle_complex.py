# PYTHON LIBS
import numpy as np
import treatment as trmt
import matplotlib.pyplot as pl

# HOMEMADE MODULES
import modes as m
import shift_angle as angle
import treatment as trmt
from shared import *

# INPUT FILE
from inputParams import *


def getAngleComplexAmp(t, amps):
    """
    t, a1 and a2 are complex values. 
    """
    a1, a2, dummy = amps

    tClipped, clippedA1, clippedA2 = clipLowAmpHilbertCriterionComplex(t, npa(a1), npa(a2))

    pl.figure()
    pl.plot(1.0e3 * tClipped, clippedA1, 's', markersize=5)
    pl.plot(1.0e3 * tClipped, clippedA2, 's', markersize=5)

    # getting clipped reconstructed signal
    pRecClipped = m.getReconstructedSignal(clippedA1, clippedA2, 0. * clippedA2, tClipped)
    mode1 = 1j
    mode1 = 1
    pRecClipped = a1 * mode1 + a2 * mode2

    pl.plot(pRecClipped)
    pl.show()

    print 'shape pRecClipped', np.shape(pRecClipped)

    # Taking shape of pRecClipped in order to look at the signifance of each
    # probe
    lines, rows = np.shape(pRecClipped)

    pl.figure()
    pl.plot(tClipped, pRecClipped[:, :])
    #   pl.show()

    # norming each reconstruction ! --> just to see
    pRecClippedNormed = []

    for l in range(lines):
        # taking the argument difference between pRecClipped and the first mode :
        # 1T
        # Arg(z' / z) = Arg(z') - Arg(z)
        # pRecClippedNormed.append(m.normVec(pRecClipped[l,:]))
        pRecClippedNormed.append(pRecClipped[l, :])
        print 'norming line', l

    print 'shape pRecClippedNormed', np.shape(pRecClippedNormed)

    pl.plot(tClipped, pRecClippedNormed)
    pl.figure()
    # pl.show()

    modePhi0 = m.vec_phi_l_from_file(1)

    ArgDiff = np.angle(m.normVec(pRecClipped) / m.normVec(modePhi0))
    ArgDiff = np.angle(((pRecClipped / pRecClipped) * m.vec_phi_l_from_file(2)) / modePhi0)

    WeightedArgDiff = []

    powa = 1

    for l in range(lines):
        sumRealPressMode = 0
        WeightedArgDiffTemp = 0

        for r in range(rows):
            sumRealPressMode += np.power(np.real(modePhi0[r]), powa)
            WeightedArgDiffTemp += ArgDiff[l, r] * np.power(np.real(modePhi0[r]), powa)

        WeightedArgDiffTemp /= sumRealPressMode
        WeightedArgDiff.append(WeightedArgDiffTemp)

    WeightedArgDiff = np.array(WeightedArgDiff)
    print 'shape WeightedArgDiff', np.shape(WeightedArgDiff)

    pl.plot(tClipped, ArgDiff[:, :], 's', markersize=5)
    pl.plot(tClipped, 180 * (1. / np.pi) * ArgDiff[:, :], 's', markersize=6)

    pl.figure()
    pl.plot(tClipped, 180 * (1. / np.pi) * WeightedArgDiff, 's', markersize=5)
    pl.legend('Weighted')
    pl.show()

    phi = []

    for th in WeightedArgDiff:
        if th < 0:
            phi.append(np.pi + th)
        else:
            phi.append(th)

    phi = np.array(phi)

    return tClipped, WeightedArgDiff, phi


def clipLowAmpHilbertCriterionComplex(t, a1C, a2C):
    """
    t, a1 and a2 are cmplx values. 
    Meaning it should be used  only with equivalent real a_k arrays.
    """
    # Giving a nice name to the condition
    k = cndtn_percent_peak_value

    pRecOnlyA1 = m.getReconstructedSignal(a1C, 0. * a2C, 0. * a1C, t)
    pRecOnlyA2 = m.getReconstructedSignal(0. * a1C, a2C, 0. * a1C, t)
    pRec = m.getReconstructedSignal(a1C, a2C, 0. * a1C, t)

    a1 = np.mean(pRecOnlyA1, axis=1)
    a2 = np.mean(pRecOnlyA2, axis=1)
    a3 = np.mean(pRec, axis=1)

    pl.figure()
    pl.plot(t, pRecOnlyA1)
    pl.figure()
    pl.plot(t, np.mean(pRecOnlyA1, axis=1))

    pl.figure()
    pl.plot(t, pRecOnlyA2)
    pl.figure()
    pl.plot(t, np.mean(pRecOnlyA2, axis=1))

    pl.figure()
    pl.plot(t, np.mean(pRec, axis=1))
    pl.show()

    # Init output variables
    tout = []
    a1out = []
    a2out = []

    ## Getting modulus of H(a), aka the enveloppe
    h_a1 = np.abs(a1)
    h_a2 = np.abs(a2)
    h_a3 = np.abs(a3)

    # epsilon vaut 1 (son maximum) quand les a vaut 0
    eps1 = (h_a1 - abs(np.real(a1))) / h_a1
    eps2 = (h_a2 - abs(np.real(a2))) / h_a2
    eps3 = (h_a3 - abs(np.real(a3))) / h_a3

    print eps3
    pl.plot(eps3)
    pl.show()

    # Applying criterion at each time
    for i, time in enumerate(t):
        if max(eps3[i], eps3[i]) < k:
            tout.append(time)
            a1out.append(a1C[i])
            a2out.append(a2C[i])

    # Converting in numpy arrays
    tout = np.array(tout)
    a1out = np.array(a1out)
    a2out = np.array(a2out)

    # Outputing complex clipped values
    return tout, a1out, a2out
