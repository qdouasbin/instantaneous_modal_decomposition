"""
IMFT
09/11/2015
Quentin Douasbin
quentin.douasbin@imft.fr
"""

import scipy.signal as sgnl
import numpy as np
from scipy import interpolate

import modes as modes

from inputParams import *

def rms(x, axis=None):
    return np.sqrt(np.mean(x ** 2, axis=axis))


def projections(amps):
    """

    :param amps: the complex values of microphones signals at a given time
    :return: the corresponding amplitude in the base [1T,1T',1R]
    """

    out = []
    for i in range(3):  # only 3 modes in the basis]

        ####### !! USE of numpy.VDOT function == complex scalar product with the conjugate on the first argument !! ######
        out.append(np.vdot(modes.vec_phi_l_from_file(i + 1), amps))

    return out


def treatData(data):
    """

    :param data: 2D array containing the pressure measurement of the eight sensors in function of time
     [ [p1, p2, ... , p8](t=0),[p1, p2, ... , p8](t=1), ..., [p1, p2, ... , p8](t=tmax)]
    :return: the corresponding amplitudes of the acoustic modes [1T,1T',1R]
    [ [a1, a2, a3](t=0),[a1, a2, a3](t=1), ..., [a1, a2, a3](t=tmax)]
    """

    out = []
    for amps in data:
        out.append(projections(amps))
    return out


def getHilbert(data):
    """
    Returns the Hilbert Transform of "data". Used to recover the enveloppes of a_l
    :param data: Array corresponding to signal to analyse
    :return: Array of data corresponding to the envelope of the signal
    """
    return np.abs(sgnl.hilbert(data))


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    """
    Filtering that does NOT change the phase of the signals
    """
    def butter_bandpass(lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = sgnl.butter(order, [low, high], btype='band')
        return b, a

    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sgnl.filtfilt(b, a, data)
    return y


def linSample(t, a):
    """
    Function that performs the fft of a' signal a
    associated to a temporal array t.
    a' is(was) defined as :
    a' = (a - a_0) / a_0
    """
    # Quadratic interpolation of the signal
    # for constant time stepping
    fa = interpolate.interp1d(t, a, kind = 'quadratic')
    xnew = np.linspace(t[0], t[-1], int(len(t) * factorSampling))
    a = fa(xnew)
    return a



def getCstSampling(temporal, data):
    l, r = np.shape(data)

    out = []
    print 'Interpolating'
    for i in range(r):
        print 'Step', i+1, 'out of', r
        out.append(trmt.linSample(temporal, data[:,i]))

    t = trmt.linSample(temporal, temporal)

    t = np.transpose(t)
    out = np.transpose(out)
    out = np.array(out)
    t = npa(t)

    return t, out

